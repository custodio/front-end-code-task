const { expect } = chai;

describe('BlockGrid', function () {
  'use strict';

  let blockGrid;
  beforeEach(() => {
    blockGrid = new BlockGrid();
  });

  describe('clearBlock', () => {
    it('should turn the block white and make it not clickable', () => {
      const block = new Block(0, 0);
      block.markBlockForDeletion();
      expect(block.colour).to.equal(colors.white);
      expect(block.clickable).to.be.false;
    });

    it('should be idempotent', () => {
      const block = new Block(0, 0);
      block.markBlockForDeletion();
      block.markBlockForDeletion();
      expect(block.colour).to.equal(colors.white);
      expect(block.clickable).to.be.false;
    });
  });

  describe('removeBlock', () => {
    it('makes the block not clickable', () => {
      blockGrid.grid[0][0].colour = 'black';
      blockGrid.grid[0][1].colour = 'black';
      const block = blockGrid.grid[0][0];

      blockGrid.blockClicked(undefined, block);

      expect(block.clickable).to.be.false;
    });

    it('turns the block white', () => {
      blockGrid.grid[0][0].colour = 'black';
      blockGrid.grid[0][1].colour = 'black';
      const block = blockGrid.grid[0][0];

      blockGrid.blockClicked(undefined, block);

      expect(block.colour).to.equal(colors.white);
    });

    it('is idempotent', () => {
      blockGrid.grid[0][0].colour = 'black';
      blockGrid.grid[0][1].colour = 'black';
      const block = blockGrid.grid[0][0];

      blockGrid.blockClicked(undefined, block);
      blockGrid.blockClicked(undefined, block);

      expect(block.clickable).to.be.false;
      expect(block.colour).to.equal(colors.white);
    });
  });

  describe('getNeighbours', () => {
    it('returns the neighbours to either side and above', () => {
      let centerBlock = new Block(1, 1);
      const surroundingBlocks = [];
      const surroundingCoordinates = [
        [1, 0],
        [0, 1],
        [2, 1],
        [1, 2],
      ];
      surroundingCoordinates.forEach((coordinates) => {
        const [x, y] = coordinates;
        let neighbour = new Block(x, y);
        surroundingBlocks.push(neighbour);
        blockGrid.grid[x][y] = neighbour;
      });
      const neighbours = blockGrid.getNeighbours(centerBlock);
      expect(neighbours.length).to.equal(4);
      surroundingCoordinates.forEach((coordinates, index) => {
        const [x, y] = coordinates;
        expect(neighbours.find((block) => block.x === x && block.y === y)).to.equal(surroundingBlocks[index]);
      });
    });

    it('returns correct results for corner blocks', () => {
      let centerBlock = new Block(0, 0);
      const surroundingBlocks = [];
      const surroundingCoordinates = [
        [1, 0],
        [0, 1],
      ];
      surroundingCoordinates.forEach((coordinates) => {
        const [x, y] = coordinates;
        const neighbour = new Block(x, y);
        surroundingBlocks.push(neighbour);
        blockGrid.grid[x][y] = neighbour;
      });
      const neighbours = blockGrid.getNeighbours(centerBlock);
      expect(neighbours.length).to.equal(2);
      surroundingCoordinates.forEach((coordinates, index) => {
        const [x, y] = coordinates;
        expect(neighbours.find((block) => block.x === x && block.y === y)).to.equal(surroundingBlocks[index]);
      });
    });

    it('returns correct results for blocks on the edges', () => {
      let centerBlock = new Block(0, 1);
      const surroundingBlocks = [];
      const surroundingCoordinates = [
        [0, 0],
        [1, 1],
        [0, 2],
      ];
      surroundingCoordinates.forEach((coordinates) => {
        const [x, y] = coordinates;
        const neighbour = new Block(x, y);
        surroundingBlocks.push(neighbour);
        blockGrid.grid[x][y] = neighbour;
      });
      const neighbours = blockGrid.getNeighbours(centerBlock);
      expect(neighbours.length).to.equal(3);
      surroundingCoordinates.forEach((coordinates, index) => {
        const [x, y] = coordinates;
        expect(neighbours.find((block) => block.x === x && block.y === y)).to.equal(surroundingBlocks[index]);
      });
    });
  });

  describe('reorderColumn', () => {
    /* Why would we need this function? Can you fill the test in and use it? */

    it('should find all the neighbour with the same color and not market for deletion', () => {
      const targetedBlock = new Block(2, 2);
      targetedBlock.colour = 'black';
      blockGrid.grid[2][2] = targetedBlock;
      const surroundingCoordinatesWithSameColour = [
        [1, 0],
        [2, 0],
        [3, 0],
        [1, 1],
        [3, 1],
        [2, 1],
        [2, 3],
      ];
      surroundingCoordinatesWithSameColour.forEach((coordinates) => {
        const [x, y] = coordinates;
        const neighbour = new Block(x, y);
        neighbour.colour = 'black';
        blockGrid.grid[x][y] = neighbour;
      });

      blockGrid.blockClicked(undefined, targetedBlock);

      surroundingCoordinatesWithSameColour.forEach((coordinates) => {
        const [x, y] = coordinates;
        expect(blockGrid.grid[x][y].colour).to.equal('#ffffff');
        expect(blockGrid.grid[x][y].clickable).to.equal(false);
      });
    });

    it('should ensure that remaining blocks are at the bottom', () => {
      const targetedBlock = new Block(2, 2);
      targetedBlock.colour = 'black';
      blockGrid.grid[2][2] = targetedBlock;
      const neighboursWithSameColour = [
        [1, 0],
        [2, 0],
        [3, 0],
        [1, 1],
        [3, 1],
        [1, 2],
        [3, 2],
      ];
      neighboursWithSameColour.forEach((coordinates) => {
        const [x, y] = coordinates;
        const neighbour = new Block(x, y);
        neighbour.colour = 'black';
        blockGrid.grid[x][y] = neighbour;
      });
      const blockThatWillRemain = new Block(2, 1);
      blockThatWillRemain.colour = 'burgundy';
      blockGrid.grid[2][1] = blockThatWillRemain;

      blockGrid.blockClicked(undefined, targetedBlock);

      expect(blockGrid.grid[2][2].colour).to.equal('burgundy');
      expect(blockGrid.grid[2][1].colour).to.equal(colors.white);
    });
  });

  describe('blockClicked', () => {
    it('should not allow to remove a single block', () => {
      const targetedBlock = new Block(0, 0);
      targetedBlock.colour = 'black';
      blockGrid.grid[0][0] = targetedBlock;

      blockGrid.blockClicked(undefined, targetedBlock);

      expect(blockGrid.grid[0][0].colour).to.equal('black');
    });
  });
});
