'use strict';

const colors = {
  white: '#ffffff',
};
const COLOURS = ['#1fc008', '#f95805', '#cf1656', '#0e0000'];
const MAX_X = 10;
const MAX_Y = 10;

const Arrays = {
  clean: (array) => array.filter(Boolean),
};

class Block {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.colour = COLOURS[Math.floor(Math.random() * COLOURS.length)];
    this.clickable = true;
  }

  markBlockForDeletion() {
    this.colour = colors.white;
    this.clickable = false;
  }
}

class BlockGrid {
  constructor() {
    this.grid = [];

    for (let x = 0; x < MAX_X; x++) {
      const col = [];
      for (let y = 0; y < MAX_Y; y++) {
        col.push(new Block(x, y));
      }

      this.grid.push(col);
    }

    return this;
  }

  redrawBlock(blockEl, block) {
    const { x, y, colour } = block;
    blockEl.id = `block_${x}x${y}`;
    blockEl.className = 'block';
    blockEl.style.background = block.colour;
  }

  getBlockByCoordinates(x, y) {
    if (x < 0 || y < 0 || x >= MAX_X || y >= MAX_Y) return;
    return this.grid[x][y];
  }

  getNeighbours(block) {
    return Arrays.clean([
      this.getBlockByCoordinates(block.x, block.y - 1),
      this.getBlockByCoordinates(block.x + 1, block.y),
      this.getBlockByCoordinates(block.x, block.y + 1),
      this.getBlockByCoordinates(block.x - 1, block.y),
    ]);
  }

  getSameColourNeighbours(block, targetedColour) {
    /*
    how do you get only same color blocks?
    */
    return this.getNeighbours(block).filter((neighbour) => targetedColour === neighbour.colour);
  }

  markNeighboursForDeletion(block, targetedColour) {
    block.markBlockForDeletion();
    const neighbours = this.getSameColourNeighbours(block, targetedColour);

    if (!neighbours.length) return;
    neighbours.forEach((neighbour) => {
      this.markNeighboursForDeletion(neighbour, targetedColour);
    });
  }

  removeEmptyBlocks() {
    for (let x = 0; x < MAX_X; x++) {
      const blocksToKeep = this.grid[x].reduce((result, current) => {
        if (current.colour !== colors.white) {
          result.push(current);
        }
        return result;
      }, []);

      const column = [];
      for (let y = MAX_Y - 1; y >= 0; y--) {
        const block = blocksToKeep.pop();
        if (block) {
          block.y = y;
          column.push(block);
        } else {
          const emptyBlock = new Block(x, y);
          emptyBlock.markBlockForDeletion();
          column.push(emptyBlock);
        }
      }
      this.grid[x] = column.reverse();
    }
  }

  render(grid = document.querySelector('#gridEl')) {
    let el = grid.cloneNode(false);
    grid.parentNode.replaceChild(el, grid);
    for (let x = 0; x < MAX_X; x++) {
      const id = 'col_' + x;
      const colEl = document.createElement('div');
      colEl.className = 'col';
      colEl.id = id;
      el.appendChild(colEl);

      for (let y = 0; y < MAX_Y; y++) {
        const block = this.grid[x][y];
        const blockEl = document.createElement('div');
        // blockEl.innerText = x + ' - ' + y;

        if (block.clickable) {
          blockEl.addEventListener('click', (evt) => this.blockClicked(evt, block));
        }

        colEl.appendChild(blockEl);
        this.redrawBlock(blockEl, block);
      }
    }

    return this;
  }

  blockClicked(e, block) {
    if (!block.clickable) return;
    const targetedColour = block.colour;

    if (!this.getSameColourNeighbours(block, targetedColour).length) {
      return;
    }

    block.markBlockForDeletion();
    this.markNeighboursForDeletion(block, targetedColour);
    /*
    what happens on each block click before you re-render?
    what happens to each column of blocks?
    */
    this.removeEmptyBlocks();
    this.render();
  }
}

window.addEventListener('DOMContentLoaded', () => new BlockGrid().render());
